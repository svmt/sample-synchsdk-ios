//
//  WTButton.swift
//  Example
//
//  Created by George on 12.01.2021.
//

import SwiftUI

struct WTButton: ButtonStyle {

    @Binding var borderColor: Color
    
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(.blue)
            .padding(.horizontal, 15)
            .overlay(RoundedRectangle(cornerRadius: 15)
                        .stroke(borderColor, lineWidth: 2.0)
                        .padding(.horizontal, 5))
    }
}

//struct WTButton_Previews: PreviewProvider {
//    static var previews: some View {
//        WTButton(title: "Say Hello World", action: {
//            print("Hello")
//        })
//    }
//}
