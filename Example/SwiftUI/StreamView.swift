//
//  StreamView.swift
//  Example
//
//  Created by George on 12.01.2021.
//

import SwiftUI
import AVKit
import SynchSDK

class StreamViewHelper: ObservableObject, SynchListener{
    var player: AVPlayer?
    private var syncSDK: SynchSDK?
    private var accessToken = "yourAccessToken"
    
    @Published var syncText: String = ""
    
    internal init() {
        syncSDK = SynchSDK(accessToken: accessToken)
        syncSDK?.attachListener(self)
    }
    
    func createSyncRoom(with id: String, clientId: String){
        syncSDK?.createGroup(id, clientName: clientId)
    }
    
    func startSync(with player: AVPlayer){
        self.player = player
        syncSDK?.startSynchronize()
    }
    
    func onClientList(clientList: [Client]) {
        
    }
    
    func onSetPlaybackRate(rate: Float) {
        player?.rate = rate
    }
    
    func onPlaybackFromPosition(position: Int) {
        player?.seek(to: CMTime(seconds: Double(position)/1000, preferredTimescale: 1))
    }
    
    func onGetPlayerPosition() -> Int {
                guard let currentDate = player?.currentItem?.currentDate() else {
                    return 0
                }
        
                let timeInterval = currentDate.timeIntervalSince1970
                return Int(timeInterval*1000)
    }
    
    func onGetPlaybackRate() -> Float {
        return player?.rate ?? 1
    }
    
    func onSyncInfo(accuracy: Float, delta: Int) {
        syncText = "accuracy: \(accuracy) delta: \(delta)"
    }
}

struct StreamView: View {
    
    var stream: Stream
    var roomId: String
    var clientId: String
    @ObservedObject var helper = StreamViewHelper()
    
    @State private var token = String()
    private let player: AVPlayer
    
    internal init(stream: Stream, roomId: String, clientId: String) {
        self.stream = stream
        self.roomId = roomId
        self.clientId = clientId
        player = AVPlayer(url: URL(string: stream.url)!)
    }
    
    var body: some View {
        ZStack{
            VideoPlayer(player: player)
                .onAppear(){
                    player.play()
                }
                .onDisappear(){
                    player.pause()
                }
            
            VStack {
                HStack {
                    Button(action: {
                        helper.createSyncRoom(with: roomId, clientId: clientId)
                        helper.startSync(with: player)
                    }, label: {
                        Text("Start Sync")
                    }).buttonStyle(WTButton(borderColor: .constant(.blue)))
                    Spacer()
                    Text(helper.syncText)
                        .foregroundColor(Color.white)
                        .background(Color.black)
                }
                
                Spacer()
            }
        }
    }
}

struct StreamView_Previews: PreviewProvider {
    static var previews: some View {
        StreamView(stream: Stream(name: "Football", url: "https://demo-app.sceenic.co/football.m3u8"),
                   roomId: "123",
                   clientId: "iPhone")
    }
}
