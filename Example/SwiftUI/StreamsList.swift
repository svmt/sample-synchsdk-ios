//
//  StreamsList.swift
//  Example
//
//  Created by George on 12.01.2021.
//

import SwiftUI

struct StreamsList: View {
    @Binding var selectedStream: Stream?
    @Environment(\.presentationMode) var presentationMode
    var streams = [
                   Stream(name: "Moctobltc", url: "https://moctobpltc-i.akamaihd.net/hls/live/571329/eight/playlist.m3u8"),
                   Stream(name: "Tears of steel", url: "https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/master.m3u8"),
                   Stream(name: "Bip bop 4X3", url: "https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8")]
    
    var body: some View {
        List(streams, id: \.name){ stream in
            Button(action: {
                selectedStream = stream
                self.presentationMode.wrappedValue.dismiss()
            }, label: {
                    Text(stream.name)
            })
        }
    }
}

struct StreamsList_Previews: PreviewProvider {
    static var previews: some View {
        StreamsList(selectedStream: .constant(nil))
    }
}

struct Stream{
    let name: String
    let url: String
}
