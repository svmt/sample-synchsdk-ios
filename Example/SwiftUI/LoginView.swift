//
//  LoginView.swift
//  Example
//
//  Created by George on 12.01.2021.
//

import SwiftUI

struct LoginView: View {
    @State private var roomId = String()
    @State private var clientName = String()
    @State private var selectedStream: Stream?
    
    @State private var roomIdColor = Color.blue
    @State private var clientNameColor = Color.blue
    @State private var selectedStreamColor = Color.blue
    
    @State private var showStreamsList = false
    @State private var pushStreamController = false
    
    var body: some View {
        NavigationView{
            VStack(alignment: .center, spacing: 12, content: {
                
                WTTextField(placeholder: "Room ID", borderColor: roomIdColor, text: $roomId)
                
                WTTextField(placeholder: "Client name", borderColor: clientNameColor, text: $clientName)
                
                HStack {
                    Button(action: {
                        showStreamsList = true
                    }) {
                        Text(selectedStream == nil ? "Select stream" : selectedStream!.name)
                    }.buttonStyle(WTButton(borderColor: $selectedStreamColor))
                    .popover(isPresented: $showStreamsList, content: {
                        StreamsList(selectedStream: $selectedStream)
                    })
                    
                    Spacer()
                }
                
                Spacer()
                
                Button("Start stream") {
                    pushStreamController = isValidRoomInfo()
                }.padding(.bottom).buttonStyle(WTButton(borderColor: .constant(.blue)))
                
                if pushStreamController{
                    NavigationLink( destination: StreamView(stream: selectedStream!, roomId: roomId, clientId: clientName), isActive: $pushStreamController) { }
                }
            })
        }
    }
    
    func isValidRoomInfo() -> Bool{
        var isValidInfo = true
        if roomId.isEmpty{
            isValidInfo = false
            roomIdColor = .red
        } else {
            roomIdColor = .blue
        }
        
        if clientName.isEmpty{
            isValidInfo = false
            clientNameColor = .red
        } else {
            clientNameColor = .blue
        }
        
        if selectedStream == nil {
            isValidInfo = false
            selectedStreamColor = .red
        } else {
            selectedStreamColor = .blue
        }
        
        return isValidInfo
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
