//
//  WTTextField.swift
//  Example
//
//  Created by George on 12.01.2021.
//

import SwiftUI

struct WTTextField: View {
    var placeholder: String
    var borderColor: Color
    @Binding var text: String
    var body: some View {
        TextField(placeholder, text: $text)
            .padding(.horizontal, 15)
            .frame(height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .overlay(RoundedRectangle(cornerRadius: 15)
                        .stroke(borderColor, lineWidth: /*@START_MENU_TOKEN@*/1.0/*@END_MENU_TOKEN@*/)
                        .padding(.horizontal, 5))
    }
}

struct WTTextField_Previews: PreviewProvider {
    static var previews: some View {
        WTTextField(placeholder: "Hello", borderColor: .blue, text: .constant(""))
    }
}
